/*
 *  s4548.h
 *
 *  Copyright (C) 2012 Brian Starkey
 *  stark3y[at]gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 *  Driver for S-4548ATC i2c LCD (101x40px)
 *
 */
#define S4548_CMD_OFF       0x00
#define S4548_CMD_PWRSV	    0x03
#define S4548_CMD_FULLON	0x05
#define S4548_CMD_SET	    0x01
#define S4548_NUM_PAGES     5
#define S4548_PAGE_WIDTH    101
#define S4548_SIZE          (S4548_PAGE_WIDTH * S4548_NUM_PAGES)

#define S4548_IOC_MAGIC 0x81

#define S4548_IOCOFF    _IO(S4548_IOC_MAGIC, S4548_CMD_OFF)
#define S4548_IOCPWRSV  _IO(S4548_IOC_MAGIC, S4548_CMD_PWRSV)
#define S4548_IOCFULLON _IO(S4548_IOC_MAGIC, S4548_CMD_FULLON)

/* TODO: May want to implement additional ioctls

extern int s4548_get_num_screens( void );

extern int s4548_lock_screen( int index );

extern int s4548_unlock_screen( int index );
*/
